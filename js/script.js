let obj = {
  name: "Вася",
  age: 33,
  skills: {
    java: "noob",
    js: "noob",
    procrastinating: ["drinks beer", "never reads manuals", "plays videogames"]
  }
}

let cloneObj = {};
for (let key in obj) {
  cloneObj[key] = obj[key];
}

console.log(obj);
console.log(cloneObj);
